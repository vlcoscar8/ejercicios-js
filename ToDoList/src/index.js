const addButton$$ = document.querySelector(".btn-add");
const input$$ = document.querySelector("input");
const listContainer$$ = document.querySelector("ul");
const message$$ = document.querySelector(".empty");

const eventListeners = () => {
    const arrTask = [];
    addButton$$.addEventListener("click", (event) => {
        event.preventDefault();

        const newElementList = document.createElement("li");
        const newContentList = document.createElement("p");
        const newDeleteButton = document.createElement("button");

        createTask(newElementList, newContentList, newDeleteButton, arrTask);

        newDeleteButton.addEventListener("click", () =>
            removeTask(newDeleteButton, arrTask)
        );

        setDefaultInput();
    });
};

const createTask = (
    newElementList,
    newContentList,
    newDeleteButton,
    arrTask
) => {
    newContentList.innerHTML = input$$.value;
    newDeleteButton.classList.add("btn-delete");
    newDeleteButton.innerHTML = "X";

    newElementList.appendChild(newContentList);
    newElementList.appendChild(newDeleteButton);
    listContainer$$.appendChild(newElementList);
    arrTask.push(input$$.value);
    existTask(arrTask);
};

const removeTask = (newDeleteButton, arrTask) => {
    newDeleteButton.parentElement.remove();
    arrTask.pop();
    existTask(arrTask);
};

const existTask = (arrTask) => {
    if (arrTask.length != 0) {
        message$$.style.display = "none";
    } else {
        message$$.style.display = "block";
    }
};

const setDefaultInput = () => {
    input$$.value = "";
};

window.onload = () => {
    eventListeners();
};
