// 7.1 Dado el siguiente array, haz una suma de todos las notas de los examenes de
// los alumnos usando la función .reduce().

const exams = [
    { name: "Yuyu Cabeza Crack", score: 5 },
    { name: "Maria Aranda Jimenez", score: 1 },
    { name: "Cristóbal Martínez Lorenzo", score: 6 },
    { name: "Mercedez Regrera Brito", score: 7 },
    { name: "Pamela Anderson", score: 3 },
    { name: "Enrique Perez Lijó", score: 6 },
    { name: "Pedro Benitez Pacheco", score: 8 },
    { name: "Ayumi Hamasaki", score: 4 },
    { name: "Robert Kiyosaki", score: 2 },
    { name: "Keanu Reeves", score: 10 },
];

let arr = [];

for (let val of exams) {
    arr.push(val.score);
}

arr.reduce((acc, el) => acc + el, 0);

// 7.2 Dado el mismo array, haz una suma de todos las notas de los examenes de los
// alumnos que esten aprobados usando la función .reduce().

arr.reduce((acc, el) => (el >= 5 ? acc + el : acc), 0);

// 7.3 Dado el mismo array, haz la media de las notas de todos los examenes .reduce().

arr.reduce((acc, el) => acc + el, 0) / (arr.length - 1);

// 6.1 Dado el siguiente javascript filtra los videojuegos por gender = 'RPG' usando
// .filter() y usa .reduce() para conseguir la media de sus .score.
// La función .find() también podría ayudarte para el contrar el genero 'RPG' en el
// array .gender.

const videogames = [
    { name: "Final Fantasy VII", genders: ["RPG"], score: 9.5 },
    {
        name: "Assasins Creed Valhala",
        genders: ["Aventura", "RPG"],
        score: 4.5,
    },
    { name: "The last of Us 2", genders: ["Acción", "Aventura"], score: 9.8 },
    { name: "Super Mario Bros", genders: ["Plataforma"], score: 8.5 },
    { name: "Genshin Impact", genders: ["RPG", "Aventura"], score: 7.5 },
    {
        name: "Legend of Zelda: Breath of the wild",
        genders: ["RPG", "La cosa más puto bonita que he visto nunca"],
        score: 10,
    },
];

const rpgScores = videogames
    .filter((el) => el.genders.includes("RPG"))
    .map((el) => el.score);
const sumScores = rpgScores.reduce((acc, el) => acc + el, 0);

console.log(sumScores / rpgScores.length);
