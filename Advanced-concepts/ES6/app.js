// Iteracion 1: Arrows;
// Crea una arrow function que tenga dos parametros a y b y
// que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre
// por consola la suma de los dos parametros.

// 1.1 Ejecuta esta función sin pasar ningún parametro
const arrow = (a = 10, b = 5) => {
    return a + b;
};
console.log(arrow());
// 1.2 Ejecuta esta función pasando un solo parametro
const arrow1 = (a = 10, b = 5) => {
    return a + b;
};
console.log(arrow1(3));
// 1.3 Ejecuta esta función pasando dos parametros
const arrow2 = (a = 10, b = 5) => {
    return a + b;
};
console.log(arrow2(1, 2));

// Iteracion 2: Destructuring
// 2.1 En base al siguiente javascript, crea variables en base a las propiedades
// del objeto usando object destructuring e imprimelas por consola. Cuidado,
// no hace falta hacer destructuring del array, solo del objeto.

const game = {
    title: "The last us 2",
    gender: ["action", "zombie", "survival"],
    year: 2020,
};

game.title = "God of War";
game.gender = ["fantasy"];
game.year = 2018;

console.log(game);

// 2.2 En base al siguiente javascript, usa destructuring para crear 3 variables
// llamadas fruit1, fruit2 y fruit3, con los valores del array. Posteriormente
// imprimelo por consola.

const fruits = ["Banana", "Strawberry", "Orange"];

const fruit1 = fruits[0];
const fruit2 = fruits[1];
const fruit3 = fruits[2];

console.log(fruit1, fruit2, fruit3);

// 2.3 En base al siguiente javascript, usa destructuring para crear 2
// variables igualandolo a la función e imprimiendolo por consola.

const animalFunction = () => {
    return { name: "Bengal Tiger", race: "Tiger" };
};

const var1 = animalFunction().name;
const var2 = animalFunction().race;

console.log(var1, var2);

// 2.4 En base al siguiente javascript, usa destructuring para crear las
// variables name y itv con sus respectivos valores. Posteriormente crea
// 3 variables usando igualmente el destructuring para cada uno de los años
// y comprueba que todo esta bien imprimiendolo.

const car = { name: "Mazda 6", itv: [2015, 2011, 2020] };
const name = car.name;
const itv = car.itv;

for (let i = 0; i < itv.length; i++) {
    console.log(itv[i]);
}

// Iteracion 3: Spread operator
// 3.1 Dado el siguiente array, crea una copia usando spread operators.
const pointsList = [32, 54, 21, 64, 75, 43];

const copia = [...pointsList];
console.log(copia);

// 3.2 Dado el siguiente objeto, crea una copia usando spread operators.
const toy = { name: "Bus laiyiar", date: "20-30-1995", color: "multicolor" };
const copiaToy = { ...toy };
console.log(copiaToy);

// 3.3 Dado los siguientes arrays, crea un nuevo array juntandolos usando
// spread operatos.
const pointsList1 = [32, 54, 21, 64, 75, 43];
const pointsList2 = [54, 87, 99, 65, 32];

const sumArr = [...pointsList1].concat([...pointsList2]);
console.log(sumArr);

// 3.4 Dado los siguientes objetos. Crea un nuevo objeto fusionando los dos
// con spread operators.
const toy1 = { name: "Bus laiyiar", date: "20-30-1995", color: "multicolor" };
const toyUpdate = { lights: "rgb", power: ["Volar like a dragon", "MoonWalk"] };

const sumObj = Object.assign({}, { ...toy1 }, { ...toyUpdate });
console.log(sumObj);

// 3.5 Dado el siguiente array. Crear una copia de él eliminando la posición 2
// pero sin editar el array inicial. De nuevo, usando spread operatos.
const colors = ["rojo", "azul", "amarillo", "verde", "naranja"];
const res = [...colors];
res.splice(2, 1);
console.log(res);

// Iteracion 4: Map

// 4.1 Dado el siguiente array, devuelve un array con sus nombres
// utilizando .map().
const users1 = [
    { id: 1, name: "Abel" },
    { id: 2, name: "Julia" },
    { id: 3, name: "Pedro" },
    { id: 4, name: "Amanda" },
];

const res1 = users1.map((el) => el.name);
console.log(res1);

// 4.2 Dado el siguiente array, devuelve una lista que contenga los valores
// de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que
// empiece por 'A'.
const users2 = [
    { id: 1, name: "Abel" },
    { id: 2, name: "Julia" },
    { id: 3, name: "Pedro" },
    { id: 4, name: "Amanda" },
];

const res2 = users2.map((el) => {
    return el.name[0] === "A" ? (el.name = "Anacleto") : (el.name = el.name);
});
console.log(res2);

// 4.3 Dado el siguiente array, devuelve una lista que contenga los valores
// de la propiedad .name y añade al valor de .name el string ' (Visitado)'
// cuando el valor de la propiedad isVisited = true.
const cities = [
    { isVisited: true, name: "Tokyo" },
    { isVisited: false, name: "Madagascar" },
    { isVisited: true, name: "Amsterdam" },
    { isVisited: false, name: "Seul" },
];

const res3 = cities.map((el) => {
    return el.isVisited ? (el.name = el.name + " (Visitado)") : el.name;
});

console.log(res3);

// Iteracion 5: Filter
// 5.1 Dado el siguiente array, utiliza .filter() para generar un nuevo array
// con los valores que sean mayor que 18
const ages1 = [22, 14, 24, 55, 65, 21, 12, 13, 90];

const result = ages1.filter((el) => el > 18);
console.log(result);

// 5.2 Dado el siguiente array, utiliza .filter() para generar un nuevo array
// con los valores que sean par.
const ages2 = [22, 14, 24, 55, 65, 21, 12, 13, 90];

const result1 = ages2.filter((el) => el % 2 === 0);
console.log(result1);

// 5.3 Dado el siguiente array, utiliza .filter() para generar un nuevo array
// con los streamers que tengan el gameMorePlayed = 'League of Legends'.
const streamers1 = [
    { name: "Rubius", age: 32, gameMorePlayed: "Minecraft" },
    { name: "Ibai", age: 25, gameMorePlayed: "League of Legends" },
    { name: "Reven", age: 43, gameMorePlayed: "League of Legends" },
    { name: "AuronPlay", age: 33, gameMorePlayed: "Among Us" },
];

const result2 = streamers1.filter(
    (el) => el.gameMorePlayed === "League of Legends"
);
console.log(result2);

// 5.4 Dado el siguiente array, utiliza .filter() para generar un nuevo array
// con los streamers que incluyan el caracter 'u' en su propiedad .name. Recomendamos
// usar la funcion .includes() para la comprobación.
const streamers2 = [
    { name: "Rubius", age: 32, gameMorePlayed: "Minecraft" },
    { name: "Ibai", age: 25, gameMorePlayed: "League of Legends" },
    { name: "Reven", age: 43, gameMorePlayed: "League of Legends" },
    { name: "AuronPlay", age: 33, gameMorePlayed: "Among Us" },
];

const result3 = streamers2.filter((el) => el.name.includes("u"));
console.log(result3);

// 5.5 utiliza .filter() para generar un nuevo array con los streamers que incluyan
// el caracter 'Legends' en su propiedad .gameMorePlayed. Recomendamos usar la funcion
// .includes() para la comprobación.
// Además, pon el valor de la propiedad .gameMorePlayed a MAYUSCULAS cuando
// .age sea mayor que 35.
const streamers3 = [
    { name: "Rubius", age: 32, gameMorePlayed: "Minecraft" },
    { name: "Ibai", age: 25, gameMorePlayed: "League of Legends" },
    { name: "Reven", age: 43, gameMorePlayed: "League of Legends" },
    { name: "AuronPlay", age: 33, gameMorePlayed: "Among Us" },
];

const arr = streamers3.filter((el) => {
    return el.gameMorePlayed.includes("Legends");
});

for (let i = 0; i < arr.length; i++) {
    arr[i].age > 35
        ? (arr[i].gameMorePlayed = arr[i].gameMorePlayed.toUpperCase())
        : arr[i];
}

console.log(arr);
