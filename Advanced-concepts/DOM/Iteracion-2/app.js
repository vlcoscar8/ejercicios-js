// 2.1 Inserta dinamicamente en un html un div vacio con javascript.
window.onload = () => addDiv();

addDiv = () => {
    let newDiv = document.createElement("div");
    let position = document.querySelector(".fn-insert-here");

    document.body.insertBefore(newDiv, position);
};

//2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

window.onload = () => addText();

addText = () => {
    let newDiv = document.createElement("div");
    let newText = document.createTextNode("Esto es un parrafo");
    newDiv.appendChild(newText);

    let position = document.querySelector(".fn-insert-here");
    document.body.insertBefore(newDiv, position);
};

// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

window.onload = () => loopText();

loopText = () => {
    let newDiv = document.createElement("div");

    let text = "";
    for (let i = 0; i < 6; i++) {
        text = text + "<p>Esto es un parrafo</p>";
    }

    let position = document.querySelector(".fn-insert-here");
    newDiv.innerHTML = `${text}`;
    document.body.insertBefore(newDiv, position);
};

// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

window.onload = () => insertText();

insertText = () => {
    let newP = document.createElement("p");
    newP.innerHTML = "Soy dinámico";

    document.body.insertBefore(newP, document.querySelector(".fn-insert-here"));
};

// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

const h2$$ = document.querySelector(".fn-insert-here");

h2$$.innerHTML = "Wubba Lubba dub dub";

// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ["Facebook", "Netflix", "Instagram", "Snapchat", "Twitter"];

const list = document.createElement("ul");

let textArr = "";

for (let i = 0; i < apps.length; i++) {
    textArr = textArr + `<li>${apps[i]}</li>`;
}

list.innerHTML = textArr;

document.body.appendChild(list);

//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

const allRemove = document.querySelectorAll(".fn-remove-me");

for (let i = 0; i < allRemove.length; i++) {
    allRemove[i].remove();
}

// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div.
// 	Recuerda que no solo puedes insertar elementos con .appendChild.

let newText = document.createElement("p");
newText.innerHTML = "Voy en medio!";

const el = document.querySelectorAll("div");
console.log(el);

document.body.insertBefore(newText, el[el.length - 1]);

// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here

const allDiv = document.querySelectorAll(".fn-insert-here");

for (let i = 0; i < allDiv.length; i++) {
    allDiv[i].innerHTML = "<p>Voy dentro!</p>";
}
