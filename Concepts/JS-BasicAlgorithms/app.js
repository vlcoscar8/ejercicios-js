// 1.1 Crea una variable llamada myFavoriteHero, asigna el valor Hulk a ella.
// 1.2 Crea una variable llamada x, asigna el valor 50 a ella.
// 1.3 Crea una variable llamada 'h' con el valor 5 y otra 'y' con el valor 10.
// 1.4 Crea una otra variable 'z' y asignale el valor de 'h' + 'y'.

const myFavoriteHero = "Hulk";
let x = 50;
let h = 5;
let y = 10;
let z = h + y;

// 1.1 Dado el siguiente javascript, cambia el valor de la propiedad age a 25.
const character = { name: "Jack Sparrow", age: 10 };

character.age = 25;
console.log(character);

// 1.2 Declara 3 variables con los nombres y valores siguientes
const firstName = "Jon";
const lastName = "Snow";
let age = 23;

// Muestralos por consola de esta forma:
("Soy Jon Snow, tengo 24 años y me gustan los lobos.");

console.log(
    "Soy " +
        firstName +
        " " +
        lastName +
        ", tengo " +
        age +
        " años y me gustan los lobos"
);

// 1.3 Dado el siguiente javascript, imprime con un console.log la suma del precio de
// ambos juguetes.
const toy1 = { name: "Buss myYear", price: 19 };
const toy2 = { name: "Rallo mcKing", price: 29 };

console.log(toy1.price + toy2.price);

// 1.4 Dado el siguiente javascript, actualiza el valor de la variable globalBasePrice a 25000
// y actualiza la propiedad finalPrice de todos los coches con el valor de su propiedad
// basePrice más el valor de la variable globalBasePrice.
let globalBasePrice = 25000;
const car1 = { name: "BMW m&m", basePrice: 50000, finalPrice: 60000 };
const car2 = { name: "Chevrolet Corbina", basePrice: 70000, finalPrice: 80000 };

car1.finalPrice = car1.basePrice + globalBasePrice;
car2.finalPrice = car2.basePrice + globalBasePrice;

console.log(car1, car2);

// 1.1 Consigue el valor "HULK" del array de cars y muestralo por consola.
const avengers1 = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

for (let avenger of avengers1) {
    if (avenger === "HULK") {
        console.log(avenger);
    }
}

// 1.2 Cambia el primer elemento de avengers a "IRONMAN"
const avengers2 = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

avengers2[0] = "IRONMAN";
console.log(avengers2);

// 1.3 Alert numero de elementos en el array usando la propiedad correcta de Array.
const avengers3 = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

alert(avengers3.length);

// 1.4 Añade 2 elementos al array: "Morty" y "Summer".
// Muestra en consola el último personaje del array
const rickAndMortyCharacters1 = ["Rick", "Beth", "Jerry"];

rickAndMortyCharacters1.push("Morty", "Summer");

console.log(rickAndMortyCharacters1[rickAndMortyCharacters1.length - 1]);

// 1.5 Elimina el último elemento del array y muestra el primero y el último por consola.
const rickAndMortyCharacters2 = [
    "Rick",
    "Beth",
    "Jerry",
    "Morty",
    "Summer",
    "Lapiz Lopez",
];

rickAndMortyCharacters2.splice(rickAndMortyCharacters2.length - 1, 1);
console.log(
    rickAndMortyCharacters2[0],
    rickAndMortyCharacters2[rickAndMortyCharacters2.length - 1]
);

// 1.6 Elimina el segundo elemento del array y muestra el array por consola.
const rickAndMortyCharacters3 = [
    "Rick",
    "Beth",
    "Jerry",
    "Morty",
    "Summer",
    "Lapiz Lopez",
];

rickAndMortyCharacters3.splice(1, 1);

console.log(rickAndMortyCharacters3);
