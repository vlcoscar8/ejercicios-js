// Iteración #1: Buscar el máximo

const mayorNum = (num1, num2) => (num1 > num2 ? num1 : num2);

mayorNum(100, 20);

// Iteración #2: Buscar la palabra más larga
const avengers = [
    "Hulk",
    "Thor",
    "IronMan",
    "Captain A.",
    "Spiderman",
    "Captain M.",
];

const palabraMasLarga = (arr) => {
    let array = [""];
    for (let i = 0; i < arr.length - 1; i++) {
        arr[i].length <= array[0].length
            ? (array[0] = array[0])
            : array.splice(0, 1, arr[i]);
    }

    return array;
};

console.log(palabraMasLarga(avengers));

// Iteración #3: Calcular la suma
const numbers = [1, 2, 3, 5, 45, 37, 58];

const suma = (arr) => {
    let res = 0;
    for (let i = 0; i < arr.length; i++) {
        res += arr[i];
    }

    return res;
};
console.log(suma(numbers));

// Iteración #4: Calcular el promedio
const numbers1 = [12, 21, 38, 5, 45, 37, 6];

const promedio = (arr) => {
    let suma = 0;

    for (let i = 0; i < arr.length; i++) {
        suma += arr[i];
    }

    return suma / arr.length;
};

console.log(promedio(numbers));

// Iteración #5: Calcular promedio de strings
const mixedElements = [6, 1, "Rayo", 1, "vallecano", "10", "upgrade", 8, "hub"];

const sumaStrings = (arr) => {
    let res = 0;
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === "number") {
            res += arr[i];
        } else if (typeof arr[i] === "string") {
            res += arr[i].length;
        } else {
            res = res;
        }
    }

    return res;
};

console.log(sumaStrings(mixedElements));

// Iteración #6: Valores únicos
const duplicates = [
    "sushi",
    "pizza",
    "burger",
    "potatoe",
    "pasta",
    "ice-cream",
    "pizza",
    "chicken",
    "onion rings",
    "pasta",
    "soda",
];

const elementosUnicos = (arr) => {
    let arrayUnicos = [];

    for (let el of arr) {
        if (!arrayUnicos.includes(el)) {
            arrayUnicos.push(el);
        }
    }

    return arrayUnicos;
};

console.log(elementosUnicos(duplicates));

// Iteración #7: Buscador de nombres
const nameFinder = [
    "Peter",
    "Steve",
    "Tony",
    "Natasha",
    "Clint",
    "Logan",
    "Xabier",
    "Bruce",
    "Peggy",
    "Jessica",
    "Marc",
];

const finder = (arr, name) => {
    let existName = true;
    let indexOfName = 0;
    if (arr.includes(name)) {
        existName = true;
        indexOfName = arr.indexOf(name);
    } else {
        existName = false;
        indexOfName = "Don't exist the name'";
    }

    return [existName, indexOfName];
};

console.log(finder(nameFinder, "Logan"));

//Iteration #8: Contador de repeticiones
const counterWords = [
    "code",
    "repeat",
    "eat",
    "sleep",
    "code",
    "enjoy",
    "sleep",
    "code",
    "enjoy",
    "upgrade",
    "code",
];

const counter = (arr) => {
    let res = [];
    arr.forEach((el) => {
        res[el] = (res[el] || 0) + 1;
    });

    return res;
};

console.log(counter(counterWords));
