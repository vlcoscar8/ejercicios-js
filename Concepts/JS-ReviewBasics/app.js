// Iteración #1: Mix for e includes
const movies = [
    { title: "Madaraspar", duration: 192, categories: ["comedia", "aventura"] },
    { title: "Spiderpan", duration: 122, categories: ["aventura", "acción"] },
    {
        title: "Solo en Whatsapp",
        duration: 223,
        categories: ["comedia", "thriller"],
    },
    {
        title: "El gato con guantes",
        duration: 111,
        categories: ["comedia", "aventura", "animación"],
    },
];

let categories = [];

for (let movie of movies) {
    for (let i = 0; i < movie.categories.length; i++) {
        !categories.includes(movie.categories[i])
            ? categories.push(movie.categories[i])
            : categories;
    }
}

console.log(categories);

// Iteración #2: Mix Fors

const users = [
    {
        name: "Manolo el del bombo",
        favoritesSounds: {
            waves: { format: "mp3", volume: 50 },
            rain: { format: "ogg", volume: 60 },
            firecamp: { format: "mp3", volume: 80 },
        },
    },
    {
        name: "Mortadelo",
        favoritesSounds: {
            waves: { format: "mp3", volume: 30 },
            shower: { format: "ogg", volume: 55 },
            train: { format: "mp3", volume: 60 },
        },
    },
    {
        name: "Super Lopez",
        favoritesSounds: {
            shower: { format: "mp3", volume: 50 },
            train: { format: "ogg", volume: 60 },
            firecamp: { format: "mp3", volume: 80 },
        },
    },
    {
        name: "El culebra",
        favoritesSounds: {
            waves: { format: "mp3", volume: 67 },
            wind: { format: "ogg", volume: 35 },
            firecamp: { format: "mp3", volume: 60 },
        },
    },
];

let countSounds = 0;
let sumVolume = 0;

for (let user of users) {
    let sounds = Object.values(user.favoritesSounds);
    for (let el of sounds) {
        sumVolume += el.volume;
        countSounds++;
    }
}

console.log(sumVolume / countSounds);

// Iteración #3: Mix Fors

const users1 = [
    {
        name: "Manolo el del bombo",
        favoritesSounds: {
            waves: { format: "mp3", volume: 50 },
            rain: { format: "ogg", volume: 60 },
            firecamp: { format: "mp3", volume: 80 },
        },
    },
    {
        name: "Mortadelo",
        favoritesSounds: {
            waves: { format: "mp3", volume: 30 },
            shower: { format: "ogg", volume: 55 },
            train: { format: "mp3", volume: 60 },
        },
    },
    {
        name: "Super Lopez",
        favoritesSounds: {
            shower: { format: "mp3", volume: 50 },
            train: { format: "ogg", volume: 60 },
            firecamp: { format: "mp3", volume: 80 },
        },
    },
    {
        name: "El culebra",
        favoritesSounds: {
            waves: { format: "mp3", volume: 67 },
            wind: { format: "ogg", volume: 35 },
            firecamp: { format: "mp3", volume: 60 },
        },
    },
];

let arrSounds = [];
let countSounds1 = [];

for (let user of users1) {
    arrSounds.push(Object.keys(user.favoritesSounds));
}

let sounds = arrSounds.flat();

sounds.forEach((el) => {
    countSounds1[el] = (countSounds1[el] || 0) + 1;
});

console.log(countSounds1);

// Iteración #4: Métodos findArrayIndex

const arrEjemplo = ["Perico", "Francisco", "Manola", "Pepita"];

const findArrayIndex = (arr, text) => {
    let res = 0;
    for (let i = 0; i < arr.length; i++) {
        arr[i] === text ? (res = i) : res;
    }

    return res;
};

console.log(findArrayIndex(arrEjemplo, "Francisco"));

// Iteración #5: Función rollDice

const rollDice = (num) => {
    return Math.floor(Math.random() * num);
};

console.log(rollDice(5));

// Iteración #6: Función swap

const arr = ["Mesirve", "Cristiano Romualdo", "Fernando Muralla", "Ronalguiño"];

const swap = (text1, text2) => {
    arr.splice(arr.indexOf(text1), 1, text2);
    arr.splice(arr.indexOf(text1), 1, text1);

    return arr;
};

console.log(swap("Mesirve", "Ronalguiño"));
