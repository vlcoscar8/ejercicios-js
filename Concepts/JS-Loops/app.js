// Iteración #1: Usa includes
const products = [
    "Camiseta de Pokemon",
    "Pantalón coquinero",
    "Gorra de gansta",
    "Camiseta de Basket",
    "Cinrurón de Orión",
    "AC/DC Camiseta",
];

const incluye = (arr, string) => {
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        arr[i].includes(string) ? res.push(arr[i]) : (res = res);
    }

    return res;
};

console.log(incluye(products, "Camiseta"));

// Iteración #2: Condicionales avanzados
const alumns = [
    { name: "Pepe Viruela", T1: false, T2: false, T3: true },
    { name: "Lucia Aranda", T1: true, T2: false, T3: true },
    { name: "Juan Miranda", T1: false, T2: true, T3: true },
    { name: "Alfredo Blanco", T1: false, T2: false, T3: false },
    { name: "Raquel Benito", T1: true, T2: true, T3: true },
];

const aprobados = (arr, student) => {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].name === student) {
            arr[i].T1 ? (count += 1) : (count = count);
            arr[i].T2 ? (count += 1) : (count = count);
            arr[i].T3 ? (count += 1) : (count = count);
        }
    }

    return count >= 2 ? true : false;
};

console.log(aprobados(alumns, "Raquel Benito"));

// Iteración #3: Probando For...of

const placesToTravel = [
    "Japon",
    "Venecia",
    "Murcia",
    "Santander",
    "Filipinas",
    "Madagascar",
];

for (let place of placesToTravel) {
    console.log(place);
}

// Iteración #4: Probando For...in
const alien = {
    name: "Wormuck",
    race: "Cucusumusu",
    planet: "Eden",
    weight: "259kg",
};

for (let info in alien) {
    console.log(info);
}

//Iteración #5: Probando For`

const placesToTravel1 = [
    { id: 5, name: "Japan" },
    { id: 11, name: "Venecia" },
    { id: 23, name: "Murcia" },
    { id: 40, name: "Santander" },
    { id: 44, name: "Filipinas" },
    { id: 59, name: "Madagascar" },
];

for (let i = 0; i < placesToTravel1.length; i++) {
    placesToTravel1[i].id === 11 || placesToTravel1[i].id === 40
        ? placesToTravel1.splice(placesToTravel1.indexOf(placesToTravel1[i]), 1)
        : placesToTravel1;
}

console.log(placesToTravel1);

//Iteración #6: Mixed For...of e includes
const toys = [
    { id: 5, name: "Buzz MyYear" },
    { id: 11, name: "Action Woman" },
    { id: 23, name: "Barbie Man" },
    { id: 40, name: "El gato con Guantes" },
    { id: 40, name: "El gato felix" },
];

for (let toy of toys) {
    for (let element of toys) {
        if (element.name.includes("gato")) {
            toys.splice(toys.indexOf(element));
        }
    }
}

console.log(toys);

// Iteración #7: For...of avanzado

const popularToys = [];
const toys1 = [
    { id: 5, name: "Buzz MyYear", sellCount: 10 },
    { id: 11, name: "Action Woman", sellCount: 24 },
    { id: 23, name: "Barbie Man", sellCount: 15 },
    { id: 40, name: "El gato con Guantes", sellCount: 8 },
    { id: 40, name: "El gato felix", sellCount: 35 },
];

for (let toy of toys1) {
    toy.sellCount > 15 ? popularToys.push(toy) : popularToys;
}

console.log(popularToys);
