// Ejercicio 1 - añadir clase

var over = () => {
    document.getElementById("logo").classList.add("background-red");
};

// // Ejercicio 2 - eiminar clase

var out = () => {
    document.querySelector(".logo").classList.remove("background-red");
};

// Ejercicio 3 - input a mayuscular

var toUpperInput = (event) => {
    event.value = event.value.toUpperCase();
};
