// * Ejercicio 2
// * → Devuelve si puedes conducir

// PRIMERA ITERACIÓN: Declara variable age

const age = 20;

// SEGUNDA ITERACIÓN: Comprobar con un condicional si puedes conducir
let res = "";
age >= 18 ? (res = "Puedes conducir") : (res = "No puedes conducir");

// TERCERA ITERACIÓN: Mostrar si puede conducir o no

console.log(res);

// * Ejercicio 3
// * → Quiero comer Pizza

// PRIMERA ITERACIÓN: Declara variable con un listado de comidas

const comidas = ["Patatas", "Tomates", "Cebollas", "Pizza", "Coliflor"];

// SEGUNDA ITERACIÓN: Recorrer el listado de comidas
// TERCERA ITERACIÓN: Si hay pizza me sirves pizza

let res2 = "";
for (let i = 0; i < comidas.length; i++) {
    comidas[i] === "Pizza" ? (res2 = "Sirveme pizza") : res2;
}

// CUARTA ITERACIÓN: Declara variable estoy a dieta

const isDieta = true;

// QUINTA ITERACIÓN: Cuando quiera pizza me sirves brocoli

for (let i = 0; i < comidas.length; i++) {
    if (comidas[i] === "Pizza" && isDieta) {
        console.log("Sirveme brocoli");
    }
}

// * Ejercicio 3
// * → E-L-R-A-Y-O-E-S-E-L-M-E-J-O-R
// */

// PRIMERA ITERACIÓN: Declara variable con 'El rayo es el mejor'
// SEGUNDA ITERACIÓN: Pasa ese texto a mayúsculas -> MDN es vuestro amigo
// TERCERA ITERACIÓN: Modifica el string -> to array MDN
// CUARTA ITERACIÓN: Añade un '-' después de cada elemento
// Devuelve en un string E-L-R-A-Y-O-E-S-E-L-M-E-J-O-R

const string = "El rayo es el mejor";

let newString = string.toUpperCase().split(" ").join("-");
console.log(newString);

/* 
* Ejercicio 4
* → La piramide - imprime por consola:
1
22
333
4444
55555
666666
7777777
88888888
999999999
*/

for (let i = 1; i < 10; i++) {
    let res = "";
    for (let j = 1; j <= i; j++) {
        res += i;
    }
    console.log(res);
}

/*
* Ejercicio 5
* → La piramide Invertida- imprime por consola:
999999999
88888888
7777777
666666
55555
4444
333
22
1
*/

for (let i = 9; i > 0; i--) {
    let res = "";
    for (let j = 1; j <= i; j++) {
        res += i;
    }
    console.log(res);
}

/*
 * Ejercicio 6 - BONUS
 * → Palíndromo: Esta función debe recibir un string y decir si es un palíndromo.
 * Un palíndromo es una frase que se lee igual al derecho que al revés.
 */

const polindromo = (string) => {
    let arr = [];
    for (let i = 0; i < string.length; i++) {
        arr.unshift(string[i]);
    }
    let res = "";
    string === arr.join("")
        ? (res = "Is polindrom")
        : (res = "Is not polindrom");

    return res;
};

console.log(polindromo("abcde fghi ihgf edcba"));
